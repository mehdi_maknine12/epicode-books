# Epicode Books

Gestione del catalogo di una biblioteca.

## Specifiche

La libreria gestisce nel catalogo libri e periodici.

Per ogni _libro_ è necessario gestire:
- Titolo
- Autori (possono essere più di uno)
- Editore
- Data di pubblicazione
- Codice ISBN (se presente)
- Codice intero della biblioteca
- Numero di copie a disposizione
- Volumi in cui è strutturato
- Lingua 
- Categoria di classificazione (anche più di una)
- Numero di pagine totali (di tutti i volumi)

Ogni _copia_ ha:
- Una posizione nella biblioteca
- Una indicazione che comunica se disponibile per il prestito o meno 
- Uno stato (in consultazione,in prestito,disponibile)

Ogni _volume_ ha:
- Titolo
- Codice ISBN(se presente)
- Codice intero
- Numero di pagine

Ogni _autore_ è caratterizzato da:
- Nome
- Cognome
- Nazionalità
- Anno di nascita
- Anno di morte (se non in vita)

I _periodici_ sono caratterizzati da:
- Data di pubblicazione
- Editore
- Titolo
- Categoria di classificazione (anche più di una)
- Periodicità (quotidiano, mensile, annuale, estemporanea, etc...)

## Permessi

Gli operatori della bliblioteca possono gestire il catalogo , gli utenti possono solo fare ricerche.



